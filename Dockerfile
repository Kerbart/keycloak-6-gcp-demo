FROM openjdk:13-alpine
RUN mkdir /app

WORKDIR /app

add keycloak-6 /app/keycloak-6

EXPOSE 8080

ENTRYPOINT ["/app/keycloak-6/bin/standalone.sh"]
